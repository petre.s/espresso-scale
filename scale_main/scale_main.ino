
// include the library code:

#include <EEPROM.h>
#include <Arduino.h>
#include <U8g2lib.h>
#include "HX711.h"

#include <SoftwareSerial.h>

#define W_N   3

#define BT_RX_PIN 5
#define BT_TX_PIN 6

SoftwareSerial bt_serial(BT_RX_PIN, BT_TX_PIN);


uint8_t upButtonPin = A2;
uint8_t downButtonPin = A1;
uint8_t tareButtonPin = A0;
uint8_t brewButtonPin = A3;
uint8_t brewPin = 12;

U8G2_SH1106_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0, A5, A4);

HX711 scale;

uint8_t scaleDataPin = PD3;
uint8_t scaleClockPin = PD2;

uint32_t start, stop;

// flow
unsigned long lastFlowReadTime = 0;
float flowW1 = 0, flowW2 = 0;
float flow = 0;
unsigned int flowIntervalMilis = 500;
unsigned long lastValidShotFlow = 0;



boolean upButton;
boolean upButtonPrev;

boolean downButton;
boolean downButtonPrev;

boolean tareButton;
boolean tareButtonPrev;

uint8_t weightTarget = 36;
uint8_t weightTargetAddress = 0;

float currentWeight = 0.0;


float w[W_N];

unsigned long w_i = 0;

boolean stopSignalUp = false;
boolean stopSignalSent = false;
unsigned long stopSignalTime = 0;
unsigned long stopSignalDurationMs = 600;


boolean upButtonPressed(){
  return upButton && (!upButtonPrev);
}


boolean downButtonPressed(){
  return downButton && (!downButtonPrev);
}

boolean tareButtonPressed(){
  return tareButton && (!tareButtonPrev);
}


void readButtons(){
  upButtonPrev = upButton;
  upButton = digitalRead(upButtonPin);
//    Serial.print("upRead:"); Serial.println(upButton);
    
  downButtonPrev = downButton;
  downButton = digitalRead(downButtonPin);
 // Serial.print("downRead:"); Serial.println(downButton);
  tareButtonPrev = tareButton;
  tareButton = digitalRead(tareButtonPin);

}

void changeModel(){
  if (upButtonPressed() && weightTarget < 255){
    weightTarget++;
    // Serial.print("up");Serial.println(weightTarget);
    EEPROM.write(weightTargetAddress, weightTarget);
  }

  if (downButtonPressed() && weightTarget > 0){
    weightTarget--;
    //  Serial.print("down:");Serial.println(weightTarget);
    EEPROM.write(weightTargetAddress, weightTarget);
  }

  
  if (currentWeight < 1.0){
    stopSignalSent = false;
  }
  unsigned long nowMillis = millis();
  
  if (stopSignalUp && (nowMillis > stopSignalTime + stopSignalDurationMs)) {
    stopSignalUp = false;
  } else  if ((currentWeight + 0.7*flow >= weightTarget) && !stopSignalSent && isDuringShot()){
  
    stopSignalSent = true;
    stopSignalUp = true;
    stopSignalTime = millis();
  }
}

void updateOutput(){

  if (tareButtonPressed()){
    scale.tare();
  }
  
  if (stopSignalUp){
    digitalWrite(brewPin, HIGH);
    Serial.print("HIGH");
  } else {
    digitalWrite(brewPin, LOW);
  }

  
}

boolean isDuringShot(){
  return lastValidShotFlow != 0 && (millis() - lastValidShotFlow > 5000);
}

// to be called after read scale
void processFlow() {
    unsigned long nowMillis =  millis();
    
    if (nowMillis - lastFlowReadTime < flowIntervalMilis){
      return;
    }
    
    
    flowW1 = flowW2;
    flowW2 = currentWeight;
 
 
    if (lastFlowReadTime != 0){
      flow = roundZero( (flowW2 -flowW1) / ((nowMillis- lastFlowReadTime) / 1000.0));  
      Serial.println(flow);
    } 
    
    lastFlowReadTime = nowMillis;

    // calculate / invalidate last lastValidShotFlow
    if (flow < 0.5 || flow > 5) {
        lastValidShotFlow = 0;
    } else if (lastValidShotFlow == 0) {
      lastValidShotFlow = nowMillis;
    }
}

void updateDisplay(){
  
  char flowStr[5];
  dtostrf(max(min(flow, 9.9), -9.9), 3, 1, flowStr);
 
  char currentWeightStr[6];
  char weightTargetStr[4];


  dtostrf(max(min(currentWeight, 99.9), -99.9), 1, 1, currentWeightStr);

  sprintf(weightTargetStr,"%d", weightTarget);

  u8g2.firstPage();
    do {
       
      u8g2.setFont(u8g2_font_profont29_mf);
      u8g2.drawStr(0,24, currentWeightStr);
      u8g2.drawStr(90,24, weightTargetStr);
      u8g2.drawStr(0,48, flowStr);
      
      if (stopSignalUp) {
        u8g2.drawStr(60, 24, "*");  
      } 
      
      progressbar(0,54,128,10, max(0, min(1.0, currentWeight / weightTarget))); 
    } while ( u8g2.nextPage() );
}

void measure(){
    w[w_i % W_N] = scale.get_units(1);
    w_i++;
    
    float sum = 0;
    for (uint8_t i= 0; i< min(W_N, w_i); i++){
      sum += w[i]; 
    }

    currentWeight = roundZero(sum / min(W_N, w_i));
}

float roundZero(float x){
   if (x >-0.1 && x < 0.1){
      return 0.0;
    } else {
      return x;
    }
}

void progressbar( int x, int y, int w, int h, float value) {
    u8g2.drawFrame(x, y, w,  h);
    u8g2.drawBox(x + 2, y + 2, (w - 4) * value,  h - 4);
}


void setup() {
  Serial.begin(9600);

  pinMode(BT_RX_PIN, INPUT);
  pinMode(BT_TX_PIN, OUTPUT);
  
  bt_serial.begin(9600);

  pinMode(upButtonPin, INPUT);
  pinMode(downButtonPin, INPUT);
  pinMode(tareButtonPin, INPUT);

  pinMode(brewPin, OUTPUT);
  
  scale.begin(scaleDataPin, scaleClockPin);
  scale.set_scale(2067.9554); // calibration
  scale.tare();
  
  u8g2.begin();

  weightTarget = EEPROM.read(weightTargetAddress);

   for (uint8_t i= 0; i< W_N; i++){
      w[i] = 0; 
    }

}

unsigned long last_bt_serial_ts = 0;

void loop() {
  measure();
  processFlow(); 
  readButtons();
  changeModel();
  updateOutput();
  updateDisplay();

  if(millis() - last_bt_serial_ts > 100){
        last_bt_serial_ts = millis();
        bt_serial.println(currentWeight);
        Serial.println(currentWeight);
    }
}
